#+title:      linux useful utils
#+date:       [2024-05-15 Wed 09:04]
#+filetags:   :linux:
#+identifier: 20240515T090455
#+STARTUP: inlineimages
#+OPTIONS: num:nil
 
* Check what's taking space

Use the ~du~ command.

To figure out what takes space in Linux and also on mac, (on mac requires ~coreutils~ for the ~sort~ option, which can be installed through Homebrew).

~du -sh ~/path/to/directory/* | sort -hr | less~

1. ~du~ is our command
2. ~-s~ is for summary, so we see the summary per line for each directory (in this case, this is what we want)
3. ~-h~ (combined with s, so no ~-~) is human readable - give me G and M I can understand, note bytes.
4. the path, followed by ~/*~ to specify everything in that directory
5. ~sort~ the sort is an "addon" from coreutils to give it further sorting. We need for:
6. ~-hr~ as above, we want sorting human units (we called it before, but without calling it again it will not sort by order) but were adding r for reverse order: biggest on top
7. ~less~ is used to be able to read through the list

we can also do ~du -sh path/to/directory/* | less~, but this way will not give us the option to reverse,  which needs ~sort~.

How to use to research what's going on:

1. Start at the home folder (unless you want to go the ~sudo~ route): ~du ~/* -csh | sort -rh | less~. 
2. The biggest directory will show at the top. Then start drilling down, specifying the paths.

For example, say the =/home/user/Documents/= is at the top with 20G. Next you run du specifying this folder in the path, which will show you what directories there take the most space. 

