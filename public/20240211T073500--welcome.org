#+title:      Welcome
#+date:       [2024-02-11 Sun 07:35]
#+filetags:   
#+identifier: 20240211T073500
#+STARTUP: inlineimages
#+OPTIONS: num:nil

* Welcome to my notes

This is where I keep my public notes. 
Previously, I've been using my [[https://taonaw.gitlab.io/taonah/][wiki-companion]] for my blog, [[https://taonaw.com][The Art of Not Asking Why]], but I want to try and switch to notes directly stored here in [[https://orgmode.org/][org-mode]] format.

** Why org-mode? Why not the wiki or just HTML?

Laziness. Or, if you want the fancier version, lack of friction. I use Emacs org-mode, which is the quickest and most efficient way for me to write. Its syntax allows me to create complicated things like footnotes, tables, properties, and more. With [[https://protesilaos.com/emacs/denote#content][Prot's Denote]], which works on top of org-mode, I can also create "meta" notes, such as this one.

Less friction means more notes. More notes mean more knowledge stored and fewer excuses not to write things down. I'm happy, you're happy.

* Where form here

[2024-04-18 Thu 12:25]:
I'm adding a time stamp to show when I updated this last.

There are two files you might be looking for if you're here:
My [[file:emacs_settings.org][Emacs configuration file]] and my [[file:tt-jtr.css][TinyTheme CSS tweaks]], for my blog.

** Notes:

- [[file:20240416T085958--customizing-edge__microsoft.org]] --> making edge work more like Arc
- [[file:20240424T080441--blogroll-in-microblog__microblog.org]] --> creating blogrolls in Micro.blog
- [[file:20240515T090455--linux-useful-utils__linux.org]] --> useful Linux commands and tools
- [[file:20240202T093235--org-mode-ids-and-links__emacs.org]] --> Linking to other headers in Emacs org-mode
- [[file:20240519T070338--checkboxes-toggle-in-org-mode__emacs.org]] --> From plain lists to checkbox lists in org-mode 
