#+title:      MS stylebook notes
#+date:       [2023-08-14 Mon 12:06]
#+filetags:   :microsoft:style:writing:
#+identifier: 20230814T120606
#+STARTUP: inlineimages

The following are notes and how to find things in the [[https://learn.microsoft.com/en-us/style-guide/][Microsoft Style Guide]]. Where certain rule does not exist, I should include it here for consistancy. 

* Verbs to use in instructions:
Search in [[https://learn.microsoft.com/en-us/style-guide/procedures-instructions/describing-interactions-with-ui][interactions with UI]].
Some verbs:

- *Select* (for the user to choose a text, highlight it, check a box, also for clicking or pressing a key)
- *Enter* (instead of set, as in "set a value to this" or "type that in", etc.)
- *Go, Go to* (we are currently using this. OK to use with "Go to the billing web page," etc.)
- *Press and Hold* Use only when physically pressing and holding a button on the keyboard: "To cancel the Terminal job, press and hold the Ctrl key while pressing the C key


* Verbs NOT to use in instructions:

Search in [[https://learn.microsoft.com/en-us/style-guide/a-z-word-list-term-collections/][A-Z word list]].

- *click* (use "select" instead)
- *log in / log out* /unless/ this is the actual button. If so, make it bold. (use "sign in" or "sign out")

* Keys

- Keyboard key names are [[https://learn.microsoft.com/en-us/style-guide/a-z-word-list-term-collections/term-collections/keys-keyboard-shortcuts][here]].
- Macis key and symbols are described [[https://support.apple.com/en-us/HT201236][here]].

* Additional, Misc rules

Here are stlye chosies I've made I want to keep consistant

** Screenshots and visual aids:

A /screenshot/ is a specific kind of an /image/; a /visual aid/ is a /screenshot/ with additional visual components such as arrows, pointers, etc.

1. Name the image file after the step in the instructions it explains, or the section. Use an underscore _ instead of space. For example, a screenshot of this section should be called ~Screenshots_and_visual_aids.png~
2. Resize in Snaggit (or editing tool of choice) and not in the publishing platform (ServiceNow or similar). This saves time and prevents mistakes.
3. *Less is more*:
   - Don't use any additional visual aids if possible. Plain screenshot should speak for themselves where possible.
   - Use steps (in Snaggit, Snap Tool) over arrows to point to the elements in the image in the order they're explained in the instructions. 
   - Use arrows over frames or circles where possible. They stand out more and are more unified in size. Use a subtle drop-shadow affect to distinguish instructiosn from the actual UI
   - Don't use additional text in images unless absolutely necessary. Usually this is an indicator of capturing too much information in one image
4. Use /CSS/ do define a single-pixel width border, black or white (to contrast the background of the page). Do not include the border as part of the image file itself.
5. Always include an [[https://en.wikipedia.org/wiki/Alt_attribute][Alt description]] of the image where possible; if not possible, also inform the developers of the platform. 


