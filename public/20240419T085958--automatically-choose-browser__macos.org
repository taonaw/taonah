#+title:      automatically choose browser
#+date:       [2024-04-19 Fri 08:59]
#+filetags:   :macos:
#+identifier: 20240419T085958
#+STARTUP: inlineimages
#+OPTIONS: num:nil toc:nil
 
Opening certain URLs with different browsers makes sense in my workflow. Some URLs are work-related and should be opened in Edge, while most URLs should be opened in Safari, and some others should be opened in Librewolf.

There are two main options I found, as demonstrated [[https://chriscoyier.net/2022/12/16/open-certain-urls-in-certain-browsers/][here]] and elsewhere: Velja and Finicky. [[https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://apps.apple.com/us/app/velja/id1607635845%3Fmt%3D12&ved=2ahUKEwjf6uGbrM6FAxVVEFkFHRWNDvMQFnoECBkQAQ&usg=AOvVaw2aCmBSs7FhQCaEAB2bbQtc][Velja is an Apple store app]], while Finicky is an open source [[https://github.com/johnste/finicky?tab=readme-ov-file][project on GitHub]], which can be installed from Homebrew.

* Velja                                                              :ATTACH:
:PROPERTIES:
:ID:       20240419T094732.822055
:END:

Most of my initial setup [[https://www.howtogeek.com/how-to-open-links-in-a-different-browser-mac/][came from this article]]. Listed below are the settings I explored (I will probably add more later):

1. Install the app
2. Give it the needed permissions
3. Once installed, it should show as a paw at the menu bar. Select if and choose settings
4. Under *General*, ensure *Launch at login* is turned on to start using it automatically
5. Under *Browsers*:
   1. *Browser*: this will choose the default browser for all URLs unless specified otherwise (see below).
   2. *Alternative browser*: Choose the browser to launch when selecting a URL while pressing and holding the Fn key (by default).
      - note: the Fn key is not always available, and it's possible to change it by selecting the question mark next to the alternative browser's choice. I chose ⌥ (the option key or Alt) instead.
   3. *Shown Browsers*: select what browsers to show from the list of available browsers when selecting Velja in the menu bar
6. Under *Rules*:
   1. select the plus at the bottom left of the window to create your first rule
   2. The *Edit Rule* window comes with several options:
      1. *Name*: user given name, doesn't affect function
      2. *Open in*: which browser should open
      3. *URL Matchers:* a complex list is available here. Notice you can add another instance by selecting the plus to apply more granular rules.
	 - *Sample URL* to enter the URL to apply the rule to (input)
	 - *Detect via*: A couple of options are available: domain, subdomain, contains, and even Regex.
	 - *Match*: this will display the result of the match from the sample URL (output)
      4. *Source Apps*: I haven't used that yet, but it appears to be an option to launch the rule under *URL Matchers* only from a certain app.

	 
[[file:data/202404/19T094732.822055/edit_rule.png]]
